import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { OktaAuthModule } from '@okta/okta-angular';

import { AppRoutingModule } from './modules/app-routing.module';
import { GraphQLModule } from './modules/graphql.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { PlayersComponent } from './views/players/players.component';
import { RankingComponent } from './views/ranking/ranking.component';

const OKTA_CONFIG = {
  issuer: 'https://dev-665798.oktapreview.com/oauth2/default',
  redirectUri: 'http://localhost:4200/implicit/callback',
  clientId: '0oajew8thaivKpzWQ0h7'
};

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PlayersComponent,
    RankingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    OktaAuthModule.initAuth(OKTA_CONFIG),
    GraphQLModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
